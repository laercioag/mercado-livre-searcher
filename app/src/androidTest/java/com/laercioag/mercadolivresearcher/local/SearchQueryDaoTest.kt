package com.laercioag.mercadolivresearcher.local

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.matcher.ViewMatchers.assertThat
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.laercioag.mercadolivresearcher.data.local.dao.SearchQueryDao
import com.laercioag.mercadolivresearcher.data.local.database.AppDatabase
import com.laercioag.mercadolivresearcher.data.local.entity.SearchQuery
import org.hamcrest.CoreMatchers.equalTo
import org.junit.After
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class SearchQueryDaoTest {

    private val context = ApplicationProvider.getApplicationContext<Context>()
    private val db: AppDatabase =
        Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java).build()
    private val searchQueryDao: SearchQueryDao = db.searchQueryDao()

    @After
    fun cleanup() {
        db.close()
    }

    @Test
    fun insertAndRetrieveSearchQuery() {
        val searchQuery = mockSearchQuery()
        searchQueryDao.insert(searchQuery)
        val list = searchQueryDao.getAll()
        assertThat(list.first(), equalTo(searchQuery))
    }

    @Test
    fun insertTwiceAndRetrieveSearchQuery() {
        val searchQuery = mockSearchQuery()
        searchQueryDao.insert(searchQuery)
        searchQueryDao.insert(searchQuery)
        val list = searchQueryDao.getAll()
        assertThat(list.first(), equalTo(searchQuery))
        assertThat(list.size, equalTo(1))
    }

    @Test
    fun insertAndDeleteSearchQuery() {
        val searchQuery = mockSearchQuery()
        searchQueryDao.insert(searchQuery)
        searchQueryDao.delete(searchQuery.query)
        val list = searchQueryDao.getAll()
        assertThat(list.size, equalTo(0))
    }

    @Test
    fun insertAndRetrieveManyRepositories() {
        val searchQueries = mockManySearchQueries()
        searchQueries.forEach(searchQueryDao::insert)
        val list = searchQueryDao.getAll()
        assertThat(list.size, equalTo(searchQueries.size))
    }

    @Test
    fun insertAndDeleteManyRepositories() {
        val searchQueries = mockManySearchQueries()
        searchQueries.forEach(searchQueryDao::insert)
        searchQueryDao.deleteAll()
        val list = searchQueryDao.getAll()
        assertThat(list.size, equalTo(0))
    }

    private fun mockManySearchQueries(size: Int = 10) =
        (1..size).map { position -> mockSearchQuery(id = position) }

    private fun mockSearchQuery(id: Int = 1) =
        SearchQuery(
            id = id,
            query = "product-$id"
        )
}