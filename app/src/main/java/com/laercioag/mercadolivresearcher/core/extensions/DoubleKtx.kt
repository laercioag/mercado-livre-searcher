package com.laercioag.mercadolivresearcher.core.extensions

import java.text.NumberFormat
import java.util.*

fun Double.formatCurrency(currencyCode: String): String =
    NumberFormat.getCurrencyInstance().apply {
        currency = Currency.getInstance(currencyCode)
    }.format(this)