package com.laercioag.mercadolivresearcher.ui.base

import dagger.android.support.DaggerAppCompatActivity

abstract class BaseActivity : DaggerAppCompatActivity()