package com.laercioag.mercadolivresearcher.ui.search

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import com.laercioag.mercadolivresearcher.R
import dagger.android.support.DaggerAppCompatDialogFragment
import javax.inject.Inject

class SearchDeleteDialogFragment : DaggerAppCompatDialogFragment() {

    companion object {
        private const val QUERY_ARGS = "QUERY_ARGS"

        fun newInstance(query: String) = SearchDeleteDialogFragment().apply {
            arguments = bundleOf(
                QUERY_ARGS to query
            )
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val searchViewModel: SearchViewModel by activityViewModels { viewModelFactory }

    private val query
        get() = arguments?.getString(QUERY_ARGS).orEmpty()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return AlertDialog.Builder(requireContext())
            .setTitle(getString(R.string.search_delete_dialog_title))
            .setNegativeButton(getString(R.string.search_delete_dialog_negative_button)) { dialogInterface, _ ->
                dialogInterface.dismiss()
            }
            .setPositiveButton(getString(R.string.search_delete_dialog_positive_button)) { _, _ ->
                searchViewModel.deleteSearchHistory(query)
            }.create()
    }
}