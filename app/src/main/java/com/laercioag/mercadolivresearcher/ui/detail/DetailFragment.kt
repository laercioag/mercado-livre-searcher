package com.laercioag.mercadolivresearcher.ui.detail

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.browser.customtabs.CustomTabsIntent
import androidx.navigation.fragment.navArgs
import com.laercioag.mercadolivresearcher.R
import com.laercioag.mercadolivresearcher.ui.base.BaseFragment
import com.laercioag.mercadolivresearcher.ui.extensions.gone
import com.laercioag.mercadolivresearcher.ui.extensions.load
import com.laercioag.mercadolivresearcher.ui.extensions.visible
import kotlinx.android.synthetic.main.detail_fragment.*

class DetailFragment : BaseFragment() {

    private val args: DetailFragmentArgs by navArgs()

    private val product
        get() = args.product

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.detail_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupThumbnail()
        setupTitle()
        setupPrice()
        setupInstallments()
        setupFreeShipping()
        setupPermalink()
    }

    private fun setupThumbnail() {
        thumbnail.load(product.thumbnail)
    }

    private fun setupTitle() {
        title.text = product.title
    }

    private fun setupPrice() {
        price.text = product.price
    }

    private fun setupInstallments() {
        installments.text = getString(
            R.string.detail_installments,
            product.installmentQuantity,
            product.installmentPrice
        )
    }

    private fun setupFreeShipping() {
        if (product.freeShipping) {
            freeShipping.visible()
        } else {
            freeShipping.gone()
        }
    }

    private fun setupPermalink() {
        actionButton.setOnClickListener {
            val builder = CustomTabsIntent.Builder()
            val customTabsIntent = builder.build()
            customTabsIntent.intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            customTabsIntent.launchUrl(requireContext(), Uri.parse(product.permalink))
        }
    }
}
