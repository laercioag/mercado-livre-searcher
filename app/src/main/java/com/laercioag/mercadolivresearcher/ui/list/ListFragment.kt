package com.laercioag.mercadolivresearcher.ui.list

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.laercioag.mercadolivresearcher.R
import com.laercioag.mercadolivresearcher.ui.base.BaseFragment
import com.laercioag.mercadolivresearcher.ui.extensions.gone
import com.laercioag.mercadolivresearcher.ui.extensions.visible
import com.laercioag.mercadolivresearcher.ui.search.SearchViewModel
import com.laercioag.mercadolivresearcher.usecase.model.Product
import kotlinx.android.synthetic.main.list_error_state.*
import kotlinx.android.synthetic.main.list_fragment.*
import kotlinx.android.synthetic.main.list_success_state.*
import javax.inject.Inject

class ListFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val searchViewModel: SearchViewModel by activityViewModels { viewModelFactory }

    private val adapter = ListAdapter(this::itemClickListener)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.list_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        searchViewModel.searchResultState.observe(
            owner = viewLifecycleOwner,
            this::handleSearchResultState
        )
        searchViewModel.searchInput.observe(owner = viewLifecycleOwner, this::handleSearchInput)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupSwipeToRefresh()
        setupRecyclerView()
        setupRetry()
        setupSearchBar()
    }

    private fun setupSwipeToRefresh() {
        swipeToRefresh.setOnRefreshListener {
            swipeToRefresh.isRefreshing = false
            searchViewModel.refreshSearch()
        }
    }

    private fun setupRecyclerView() {
        val layoutManager = LinearLayoutManager(context)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter
        recyclerView.addItemDecoration(
            DividerItemDecoration(
                recyclerView.context,
                layoutManager.orientation
            )
        )
    }

    private fun setupRetry() {
        retryText.setOnClickListener {
            searchViewModel.refreshSearch()
        }
    }

    private fun setupSearchBar() {
        searchBar.setOnClickListener {
            findNavController().navigate(ListFragmentDirections.actionListFragmentToSearchFragment())
        }
    }

    private fun handleSearchResultState(state: SearchViewModel.SearchResultState) {
        when (state) {
            is SearchViewModel.SearchResultState.Loading -> setupLoadingState()
            is SearchViewModel.SearchResultState.Error -> setupErrorState(state.throwable)
            is SearchViewModel.SearchResultState.Empty -> setupEmptyState()
            is SearchViewModel.SearchResultState.Success -> setupSuccessState(state.products)
        }
    }

    private fun setupLoadingState() {
        errorStateLayout.gone()
        emptyStateLayout.gone()
        successStateLayout.gone()
        loadingStateLayout.visible()
        recyclerView.scrollToPosition(0)
    }

    private fun setupEmptyState() {
        errorStateLayout.gone()
        successStateLayout.gone()
        loadingStateLayout.gone()
        emptyStateLayout.visible()
    }

    private fun setupErrorState(throwable: Throwable) {
        successStateLayout.gone()
        loadingStateLayout.gone()
        emptyStateLayout.gone()
        errorStateLayout.visible()
        Log.e(ListFragment::class.java.simpleName, "Error: ", throwable)
    }

    private fun setupSuccessState(products: List<Product>) {
        loadingStateLayout.gone()
        emptyStateLayout.gone()
        errorStateLayout.gone()
        successStateLayout.visible()
        adapter.items = products
    }

    private fun handleSearchInput(searchQuery: String?) {
        if (searchQuery.isNullOrBlank()) {
            searchInput.text = getString(R.string.search_input_hint)
        } else {
            searchInput.text = searchQuery
        }
    }

    private fun itemClickListener(product: Product) {
        findNavController().navigate(
            ListFragmentDirections.actionListFragmentToDetailFragment(
                product
            )
        )
    }
}
