package com.laercioag.mercadolivresearcher.ui.search

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.laercioag.mercadolivresearcher.usecase.DeleteSearchHistoryUseCase
import com.laercioag.mercadolivresearcher.usecase.GetSearchHistoryUseCase
import com.laercioag.mercadolivresearcher.usecase.SearchUseCase
import com.laercioag.mercadolivresearcher.usecase.model.Product
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class SearchViewModel @Inject constructor(
    private val searchUseCase: SearchUseCase,
    private val getSearchHistoryUseCase: GetSearchHistoryUseCase,
    private val deleteSearchHistoryUseCase: DeleteSearchHistoryUseCase
) : ViewModel() {

    val searchInput = MutableLiveData<String>()
    private val _searchResultState = MutableLiveData<SearchResultState>()
    val searchResultState: LiveData<SearchResultState> = _searchResultState
    private val _searchHistoryState = MutableLiveData<SearchHistoryState>()
    val searchHistoryState: LiveData<SearchHistoryState> = _searchHistoryState

    sealed class SearchResultState {
        data class Error(val throwable: Throwable) : SearchResultState()
        object Loading : SearchResultState()
        object Empty : SearchResultState()
        data class Success(val products: List<Product>) : SearchResultState()
    }

    sealed class SearchHistoryState {
        data class Error(val throwable: Throwable) : SearchHistoryState()
        data class Success(val searchHistory: List<String>) : SearchHistoryState()
    }

    init {
        searchInput.observeForever(this::search)
    }

    fun refreshSearch() {
        searchInput.value?.run { search(this) }
    }

    private fun search(searchQuery: String?) {
        if (!searchQuery.isNullOrBlank()) {
            searchUseCase.execute(searchQuery)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnSubscribe {
                    _searchResultState.value = SearchResultState.Loading
                }
                .subscribeBy(
                    onSuccess = { list ->
                        if (list.isEmpty()) {
                            _searchResultState.value = SearchResultState.Empty
                        } else {
                            _searchResultState.value = SearchResultState.Success(list)
                        }
                    },
                    onError = { _searchResultState.value = SearchResultState.Error(it) }
                )
        }
    }

    fun getSearchHistory() {
        getSearchHistoryUseCase.execute()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribeBy(
                onSuccess = { searchHistory ->
                    _searchHistoryState.value = SearchHistoryState.Success(searchHistory)
                },
                onError = {
                    _searchHistoryState.value = SearchHistoryState.Error(it)
                }
            )
    }

    fun deleteSearchHistory(query: String) {
        deleteSearchHistoryUseCase.execute(query)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribeBy(
                onComplete = {
                    getSearchHistory()
                },
                onError = {
                    Log.e(SearchViewModel::class.simpleName, "Error:", it)
                }
            )
    }
}
