package com.laercioag.mercadolivresearcher.ui.search

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.laercioag.mercadolivresearcher.R
import com.laercioag.mercadolivresearcher.ui.base.BaseFragment
import com.laercioag.mercadolivresearcher.ui.extensions.hideKeyboard
import com.laercioag.mercadolivresearcher.ui.extensions.requestFocusAndShowKeyboard
import kotlinx.android.synthetic.main.search_fragment.*
import javax.inject.Inject

class SearchFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val searchViewModel: SearchViewModel by activityViewModels { viewModelFactory }

    private val adapter = SearchAdapter(
        itemCLickListener = this::itemClickListener,
        itemLongCLickListener = this::itemLongClickListener
    )

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.search_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        searchViewModel.searchHistoryState.observe(
            owner = viewLifecycleOwner,
            this::handleSearchHistoryState
        )
        searchViewModel.getSearchHistory()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupSearchInput()
        setupRecyclerView()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        hideKeyboard()
    }

    private fun setupSearchInput() {
        val currentSearchQuery = searchViewModel.searchInput.value
        if (currentSearchQuery.isNullOrBlank()) {
            searchInput.text = null
        } else {
            searchInput.setText(currentSearchQuery)
        }
        searchInput.setOnEditorActionListener { textView, i, _ ->
            if (i == EditorInfo.IME_ACTION_GO) {
                setSearchInput(textView.text.toString())
                return@setOnEditorActionListener true
            }
            return@setOnEditorActionListener false
        }
        searchInput.requestFocusAndShowKeyboard()
    }

    private fun setSearchInput(query: String) {
        searchViewModel.searchInput.postValue(query)
        findNavController().popBackStack()
    }

    private fun setupRecyclerView() {
        val layoutManager = LinearLayoutManager(context)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter
        recyclerView.addItemDecoration(
            DividerItemDecoration(
                recyclerView.context,
                layoutManager.orientation
            )
        )
    }

    private fun itemClickListener(query: String) {
        setSearchInput(query)
    }

    private fun itemLongClickListener(query: String) {
        SearchDeleteDialogFragment.newInstance(query).show(
            childFragmentManager,
            SearchDeleteDialogFragment::class.java.simpleName
        )
    }

    private fun handleSearchHistoryState(state: SearchViewModel.SearchHistoryState) {
        when (state) {
            is SearchViewModel.SearchHistoryState.Success -> setupSearchHistorySuccessState(state.searchHistory)
            is SearchViewModel.SearchHistoryState.Error -> setupSearchHistoryErrorState(state.throwable)
        }
    }

    private fun setupSearchHistorySuccessState(searchHistory: List<String>) {
        adapter.items = searchHistory
    }

    private fun setupSearchHistoryErrorState(throwable: Throwable) {
        Log.e(SearchFragment::class.java.simpleName, "Error: ", throwable)
    }
}
