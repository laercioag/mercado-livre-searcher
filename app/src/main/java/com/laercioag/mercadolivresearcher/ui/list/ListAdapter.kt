package com.laercioag.mercadolivresearcher.ui.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.laercioag.mercadolivresearcher.R
import com.laercioag.mercadolivresearcher.ui.extensions.load
import com.laercioag.mercadolivresearcher.usecase.model.Product
import kotlinx.android.synthetic.main.list_item.view.*

class ListAdapter(
    private val itemCLickListener: (Product) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var items = listOf<Product>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            R.layout.list_item -> ItemViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.list_item,
                    parent,
                    false
                ),
                itemCLickListener
            )
            else -> throw IllegalArgumentException("Unknown view type $viewType")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = items[position]
        when (val viewType = getItemViewType(position)) {
            R.layout.list_item -> (holder as ItemViewHolder).bind(item)
            else -> throw IllegalArgumentException("Unknown view type $viewType")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.list_item
    }

    override fun getItemCount(): Int = items.size

    class ItemViewHolder(itemView: View, private val itemCLickListener: (Product) -> Unit) :
        RecyclerView.ViewHolder(itemView) {
        fun bind(item: Product) {
            with(itemView) {
                thumbnail.load(item.thumbnail)
                name.text = item.title
                price.text = item.price
                setOnClickListener {
                    itemCLickListener(item)
                }
            }
        }
    }
}