package com.laercioag.mercadolivresearcher.ui.extensions

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager

fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}

fun View.requestFocusAndShowKeyboard() {
    val imm: InputMethodManager? =
        context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
    requestFocus()
    imm?.showSoftInput(this, 0)
}