package com.laercioag.mercadolivresearcher.ui.search

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.laercioag.mercadolivresearcher.R
import kotlinx.android.synthetic.main.search_item.view.*

class SearchAdapter(
    private val itemCLickListener: (String) -> Unit,
    private val itemLongCLickListener: (String) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var items = listOf<String>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            R.layout.search_item -> ItemViewHolder(
                itemView = LayoutInflater.from(parent.context).inflate(
                    R.layout.search_item,
                    parent,
                    false
                ),
                itemCLickListener = itemCLickListener,
                itemLongCLickListener = itemLongCLickListener
            )
            else -> throw IllegalArgumentException("Unknown view type $viewType")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = items[position]
        when (val viewType = getItemViewType(position)) {
            R.layout.search_item -> (holder as ItemViewHolder).bind(item)
            else -> throw IllegalArgumentException("Unknown view type $viewType")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.search_item
    }

    override fun getItemCount(): Int = items.size

    class ItemViewHolder(
        itemView: View,
        private val itemCLickListener: (String) -> Unit,
        private val itemLongCLickListener: (String) -> Unit
    ) :
        RecyclerView.ViewHolder(itemView) {
        fun bind(item: String) {
            with(itemView) {
                title.text = item
                setOnClickListener {
                    itemCLickListener(item)
                }
                setOnLongClickListener {
                    itemLongCLickListener(item)
                    true
                }
            }
        }
    }
}