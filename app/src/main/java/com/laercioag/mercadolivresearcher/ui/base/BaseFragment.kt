package com.laercioag.mercadolivresearcher.ui.base

import dagger.android.support.DaggerFragment

abstract class BaseFragment : DaggerFragment()