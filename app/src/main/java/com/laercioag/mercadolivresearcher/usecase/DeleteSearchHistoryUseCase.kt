package com.laercioag.mercadolivresearcher.usecase

import com.laercioag.mercadolivresearcher.data.repository.Repository
import io.reactivex.Completable
import javax.inject.Inject

interface DeleteSearchHistoryUseCase {
    fun execute(query: String): Completable
}

class DeleteSearchHistoryUseCaseImpl @Inject constructor(
    private val repository: Repository
) : DeleteSearchHistoryUseCase {
    override fun execute(query: String): Completable =
        repository.deleteSearchQuery(query)
}