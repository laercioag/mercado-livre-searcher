package com.laercioag.mercadolivresearcher.usecase.mapper

import com.laercioag.mercadolivresearcher.core.extensions.formatCurrency
import com.laercioag.mercadolivresearcher.data.remote.dto.ProductDTO
import com.laercioag.mercadolivresearcher.usecase.model.Product
import javax.inject.Inject

interface ProductMapper {
    fun mapTo(from: ProductDTO): Product
}

class ProductMapperImpl @Inject constructor() : ProductMapper {

    override fun mapTo(from: ProductDTO): Product =
        with(from) {
            Product(
                id = id,
                title = title,
                thumbnail = thumbnail,
                permalink = permalink,
                price = price.formatCurrency(currencyId),
                installmentPrice = installments.amount.formatCurrency(installments.currencyId),
                installmentQuantity = installments.quantity,
                freeShipping = shipping.freeShipping
            )
        }
}
