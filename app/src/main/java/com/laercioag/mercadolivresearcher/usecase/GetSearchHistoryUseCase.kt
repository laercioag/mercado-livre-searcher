package com.laercioag.mercadolivresearcher.usecase

import com.laercioag.mercadolivresearcher.data.repository.Repository
import io.reactivex.Single
import javax.inject.Inject

interface GetSearchHistoryUseCase {
    fun execute(): Single<List<String>>
}

class GetSearchHistoryUseCaseImpl @Inject constructor(
    private val repository: Repository
) : GetSearchHistoryUseCase {
    override fun execute(): Single<List<String>> =
        repository.getSearchHistory()
}