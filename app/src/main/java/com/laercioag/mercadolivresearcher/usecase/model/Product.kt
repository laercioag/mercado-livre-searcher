package com.laercioag.mercadolivresearcher.usecase.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Product(
    val id: String,
    val title: String,
    val price: String,
    val permalink: String,
    val thumbnail: String,
    val installmentPrice: String,
    val installmentQuantity: Int,
    val freeShipping: Boolean
) : Parcelable