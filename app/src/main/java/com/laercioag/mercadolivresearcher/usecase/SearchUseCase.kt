package com.laercioag.mercadolivresearcher.usecase

import com.laercioag.mercadolivresearcher.data.repository.Repository
import com.laercioag.mercadolivresearcher.usecase.mapper.ProductMapper
import com.laercioag.mercadolivresearcher.usecase.model.Product
import io.reactivex.Single
import javax.inject.Inject

interface SearchUseCase {
    fun execute(query: String): Single<List<Product>>
}

class SearchUseCaseImpl @Inject constructor(
    private val repository: Repository,
    private val productMapper: ProductMapper
) : SearchUseCase {
    override fun execute(query: String): Single<List<Product>> =
        repository.search(query).map { it.map(productMapper::mapTo) }
}