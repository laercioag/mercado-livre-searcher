package com.laercioag.mercadolivresearcher.di.module

import com.laercioag.mercadolivresearcher.ui.detail.DetailFragment
import com.laercioag.mercadolivresearcher.ui.list.ListFragment
import com.laercioag.mercadolivresearcher.ui.main.MainActivity
import com.laercioag.mercadolivresearcher.ui.search.SearchDeleteDialogFragment
import com.laercioag.mercadolivresearcher.ui.search.SearchFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class AndroidModule {

    @ContributesAndroidInjector
    abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun bindListFragment(): ListFragment

    @ContributesAndroidInjector
    abstract fun bindSearchFragment(): SearchFragment

    @ContributesAndroidInjector
    abstract fun bindDetailFragment(): DetailFragment

    @ContributesAndroidInjector
    abstract fun bindSearchDeleteDialogFragment(): SearchDeleteDialogFragment
}