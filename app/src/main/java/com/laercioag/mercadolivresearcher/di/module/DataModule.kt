package com.laercioag.mercadolivresearcher.di.module

import android.content.Context
import androidx.room.Room
import com.laercioag.mercadolivresearcher.data.local.database.AppDatabase
import com.laercioag.mercadolivresearcher.data.remote.api.Api
import com.laercioag.mercadolivresearcher.data.remote.api.ApiImpl
import com.laercioag.mercadolivresearcher.data.remote.service.RemoteService
import com.laercioag.mercadolivresearcher.data.repository.Repository
import com.laercioag.mercadolivresearcher.data.repository.RepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module(includes = [DataModule.Binder::class])
class DataModule {

    companion object {
        private const val CLIENT_BASE_URL = "https://api.mercadolibre.com/sites/"
    }

    @Provides
    @Singleton
    fun provideClient(): OkHttpClient =
        OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            })
            .build()

    @Provides
    @Singleton
    fun provideRetrofit(client: OkHttpClient): Retrofit =
        Retrofit.Builder()
            .client(client)
            .baseUrl(CLIENT_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()

    @Provides
    @Singleton
    fun provideRemoteService(retrofit: Retrofit): RemoteService =
        retrofit.create(RemoteService::class.java)

    @Provides
    @Singleton
    fun provideDatabase(@AppContext context: Context): AppDatabase =
        Room.databaseBuilder(
            context,
            AppDatabase::class.java, "database"
        ).build()

    @Module
    abstract class Binder {
        @Binds
        abstract fun bindApi(impl: ApiImpl): Api

        @Binds
        abstract fun bindRepository(impl: RepositoryImpl): Repository
    }
}