package com.laercioag.mercadolivresearcher.di.component

import com.laercioag.mercadolivresearcher.di.module.*
import com.laercioag.mercadolivresearcher.ui.base.BaseApplication
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AndroidSupportInjectionModule::class,
        AppModule::class,
        AndroidModule::class,
        ViewModelModule::class,
        DataModule::class,
        UseCaseModule::class
    ]
)
interface ApplicationComponent : AndroidInjector<BaseApplication> {

    @Component.Factory
    interface Factory : AndroidInjector.Factory<BaseApplication>
}