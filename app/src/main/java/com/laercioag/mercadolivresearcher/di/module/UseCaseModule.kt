package com.laercioag.mercadolivresearcher.di.module

import com.laercioag.mercadolivresearcher.usecase.*
import com.laercioag.mercadolivresearcher.usecase.mapper.ProductMapper
import com.laercioag.mercadolivresearcher.usecase.mapper.ProductMapperImpl
import dagger.Binds
import dagger.Module

@Module
abstract class UseCaseModule {

    @Binds
    abstract fun bindProductMapper(impl: ProductMapperImpl): ProductMapper

    @Binds
    abstract fun bindSearchUseCase(impl: SearchUseCaseImpl): SearchUseCase

    @Binds
    abstract fun bindGetSearchHistoryUseCase(impl: GetSearchHistoryUseCaseImpl): GetSearchHistoryUseCase

    @Binds
    abstract fun bindDeleteSearchHistoryUseCase(impl: DeleteSearchHistoryUseCaseImpl): DeleteSearchHistoryUseCase
}