package com.laercioag.mercadolivresearcher.data.remote.dto

import com.google.gson.annotations.SerializedName

data class InstallmentsDTO(
    val quantity: Int,
    val amount: Double,
    @SerializedName("currency_id") val currencyId: String
)