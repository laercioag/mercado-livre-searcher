package com.laercioag.mercadolivresearcher.data.remote.api

import com.laercioag.mercadolivresearcher.data.remote.dto.SearchResponseDTO
import com.laercioag.mercadolivresearcher.data.remote.service.RemoteService
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

interface Api {
    fun search(searchQuery: String): Single<SearchResponseDTO>
}

@Singleton
class ApiImpl @Inject constructor(
    private val remoteService: RemoteService
) : Api {

    override fun search(searchQuery: String): Single<SearchResponseDTO> {
        return remoteService.search(searchQuery)
    }
}