package com.laercioag.mercadolivresearcher.data.remote.dto

data class SearchResponseDTO(
    val results: List<ProductDTO>
)