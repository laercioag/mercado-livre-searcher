package com.laercioag.mercadolivresearcher.data.remote.dto

import com.google.gson.annotations.SerializedName

data class ProductDTO(
    val id: String,
    val title: String,
    val price: Double,
    val permalink: String,
    val thumbnail: String,
    @SerializedName("currency_id") val currencyId: String,
    val installments: InstallmentsDTO,
    val shipping: ShippingDTO
)