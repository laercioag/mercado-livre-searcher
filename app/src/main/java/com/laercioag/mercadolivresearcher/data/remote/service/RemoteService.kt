package com.laercioag.mercadolivresearcher.data.remote.service

import com.laercioag.mercadolivresearcher.data.remote.dto.SearchResponseDTO
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface RemoteService {

    @GET("MLB/search")
    fun search(@Query("q") searchQuery: String): Single<SearchResponseDTO>
}