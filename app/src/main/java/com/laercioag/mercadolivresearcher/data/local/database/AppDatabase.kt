package com.laercioag.mercadolivresearcher.data.local.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.laercioag.mercadolivresearcher.data.local.dao.SearchQueryDao
import com.laercioag.mercadolivresearcher.data.local.entity.SearchQuery

@Database(entities = [SearchQuery::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun searchQueryDao(): SearchQueryDao
}