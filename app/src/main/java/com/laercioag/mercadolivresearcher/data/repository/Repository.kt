package com.laercioag.mercadolivresearcher.data.repository

import com.laercioag.mercadolivresearcher.data.local.database.AppDatabase
import com.laercioag.mercadolivresearcher.data.local.entity.SearchQuery
import com.laercioag.mercadolivresearcher.data.remote.api.Api
import com.laercioag.mercadolivresearcher.data.remote.dto.ProductDTO
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

interface Repository {
    fun search(query: String): Single<List<ProductDTO>>
    fun getSearchHistory(): Single<List<String>>
    fun deleteSearchQuery(query: String): Completable
}

@Singleton
class RepositoryImpl @Inject constructor(
    private val api: Api,
    private val database: AppDatabase
) : Repository {

    override fun search(query: String): Single<List<ProductDTO>> =
        api.search(query)
            .doOnSubscribe {
                database.searchQueryDao().insert(SearchQuery(query = query))
            }
            .map { it.results }

    override fun getSearchHistory(): Single<List<String>> =
        Single.fromCallable {
            database.searchQueryDao().getAll().map { it.query }
        }

    override fun deleteSearchQuery(query: String): Completable =
        Completable.fromCallable {
            database.searchQueryDao().delete(query)
        }
}