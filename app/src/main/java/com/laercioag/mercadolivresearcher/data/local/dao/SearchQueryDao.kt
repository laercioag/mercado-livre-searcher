package com.laercioag.mercadolivresearcher.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.laercioag.mercadolivresearcher.data.local.entity.SearchQuery

@Dao
interface SearchQueryDao {

    @Query("SELECT * FROM searchquery ORDER BY id DESC")
    fun getAll(): List<SearchQuery>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(searchQuery: SearchQuery)

    @Query("DELETE FROM searchquery WHERE `query` = :query")
    fun delete(query: String)

    @Query("DELETE FROM searchquery")
    fun deleteAll()
}