package com.laercioag.mercadolivresearcher.data.remote.dto

import com.google.gson.annotations.SerializedName

data class ShippingDTO(
    @SerializedName("free_shipping") val freeShipping: Boolean
)