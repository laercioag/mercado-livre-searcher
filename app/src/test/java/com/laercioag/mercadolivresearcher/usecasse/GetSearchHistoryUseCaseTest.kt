package com.laercioag.mercadolivresearcher.usecasse

import com.laercioag.mercadolivresearcher.data.repository.Repository
import com.laercioag.mercadolivresearcher.usecase.GetSearchHistoryUseCaseImpl
import com.nhaarman.mockitokotlin2.*
import io.reactivex.Single
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class GetSearchHistoryUseCaseTest {

    private val repository: Repository = mock()
    private val getSearchHistoryUseCase = GetSearchHistoryUseCaseImpl(repository)

    @Test
    fun `should get search history given that repository returns as expected`() {
        val searchHistoryStrings = listOf("query")
        whenever(repository.getSearchHistory()).thenReturn(Single.just(searchHistoryStrings))
        getSearchHistoryUseCase.execute().test()
            .assertNoErrors()
            .assertValue(searchHistoryStrings)
            .assertComplete()
        verify(repository, times(1)).getSearchHistory()
        verifyNoMoreInteractions(repository)
    }

    @Test
    fun `should fail to get search history given that repository returns an error`() {
        val error = RuntimeException()
        whenever(repository.getSearchHistory()).thenReturn(Single.error(error))
        getSearchHistoryUseCase.execute().test()
            .assertError(error)
            .assertNoValues()
            .assertNotComplete()
        verify(repository, times(1)).getSearchHistory()
        verifyNoMoreInteractions(repository)
    }
}
