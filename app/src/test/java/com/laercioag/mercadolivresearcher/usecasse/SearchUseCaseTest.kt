package com.laercioag.mercadolivresearcher.usecasse

import com.laercioag.mercadolivresearcher.data.remote.dto.ProductDTO
import com.laercioag.mercadolivresearcher.data.repository.Repository
import com.laercioag.mercadolivresearcher.usecase.SearchUseCaseImpl
import com.laercioag.mercadolivresearcher.usecase.mapper.ProductMapper
import com.laercioag.mercadolivresearcher.usecase.model.Product
import com.nhaarman.mockitokotlin2.*
import io.reactivex.Single
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.ArgumentMatchers.anyString

@RunWith(JUnit4::class)
class SearchUseCaseTest {

    private val repository: Repository = mock()
    private val mapper: ProductMapper = mock()
    private val searchUseCase = SearchUseCaseImpl(repository, mapper)

    @Test
    fun `should get search results given that repository returns as expected`() {
        val productDTO: ProductDTO = mock()
        val response = listOf(productDTO)
        val product: Product = mock()
        val products = listOf(product)
        whenever(mapper.mapTo(any())).thenReturn(product)
        whenever(repository.search(anyString())).thenReturn(Single.just(response))
        searchUseCase.execute("query").test()
            .assertNoErrors()
            .assertValue(products)
            .assertComplete()
        verify(mapper, times(1)).mapTo(productDTO)
        verify(repository, times(1)).search("query")
        verifyNoMoreInteractions(repository)
    }

    @Test
    fun `should fail to get search results given that repository returns an error`() {
        val error = Exception()
        whenever(repository.search(anyString())).thenReturn(Single.error(error))
        searchUseCase.execute("query").test()
            .assertError(error)
            .assertNoValues()
            .assertNotComplete()
        verify(repository, times(1)).search("query")
        verifyNoMoreInteractions(repository)
    }

    @Test
    fun `should fail to search results given that mapper returns an error`() {
        val productDTO: ProductDTO = mock()
        val response = listOf(productDTO)
        val error = RuntimeException()
        doThrow(error).whenever(mapper).mapTo(any())
        whenever(repository.search(anyString())).thenReturn(Single.just(response))
        searchUseCase.execute("query").test()
            .assertError(error)
            .assertNoValues()
            .assertNotComplete()
        verify(mapper, times(1)).mapTo(productDTO)
        verify(repository, times(1)).search("query")
        verifyNoMoreInteractions(repository)
    }
}
