package com.laercioag.mercadolivresearcher.usecasse

import com.laercioag.mercadolivresearcher.data.repository.Repository
import com.laercioag.mercadolivresearcher.usecase.DeleteSearchHistoryUseCaseImpl
import com.nhaarman.mockitokotlin2.*
import io.reactivex.Completable
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.ArgumentMatchers.anyString

@RunWith(JUnit4::class)
class DeleteSearchHistoryUseCaseTest {

    private val repository: Repository = mock()
    private val deleteSearchHistoryUseCase = DeleteSearchHistoryUseCaseImpl(repository)

    @Test
    fun `should delete search history given that repository returns as expected`() {
        whenever(repository.deleteSearchQuery(anyString())).thenReturn(Completable.complete())
        deleteSearchHistoryUseCase.execute("query").test()
            .assertNoErrors()
            .assertNoValues()
            .assertComplete()
        verify(repository, times(1)).deleteSearchQuery("query")
        verifyNoMoreInteractions(repository)
    }

    @Test
    fun `should fail to delete search history given that repository returns an error`() {
        val error = RuntimeException()
        whenever(repository.deleteSearchQuery(anyString())).thenReturn(Completable.error(error))
        deleteSearchHistoryUseCase.execute("query").test()
            .assertError(error)
            .assertNoValues()
            .assertNotComplete()
        verify(repository, times(1)).deleteSearchQuery("query")
        verifyNoMoreInteractions(repository)
    }
}
