package com.laercioag.mercadolivresearcher.repository

import com.laercioag.mercadolivresearcher.data.local.dao.SearchQueryDao
import com.laercioag.mercadolivresearcher.data.local.database.AppDatabase
import com.laercioag.mercadolivresearcher.data.local.entity.SearchQuery
import com.laercioag.mercadolivresearcher.data.remote.api.Api
import com.laercioag.mercadolivresearcher.data.remote.dto.SearchResponseDTO
import com.laercioag.mercadolivresearcher.data.repository.RepositoryImpl
import com.nhaarman.mockitokotlin2.*
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.ArgumentMatchers.anyString

@RunWith(JUnit4::class)
class RepositoryTest {

    private val api: Api = mock()
    private val database: AppDatabase = mock()
    private val searchQueryDao: SearchQueryDao = mock()
    private val repository = RepositoryImpl(api, database)

    @Before
    fun setup() {
        whenever(database.searchQueryDao()).thenReturn(searchQueryDao)
    }

    @Test
    fun `should search successfully given that api returns as expected`() {
        val searchResponse: SearchResponseDTO = mock()
        whenever(api.search(anyString())).thenReturn(Single.just(searchResponse))
        doNothing().whenever(searchQueryDao).insert(any())
        repository.search("query")
            .test()
            .assertValue(searchResponse.results)
            .assertNoErrors()
            .assertComplete()
        verify(api, times(1)).search("query")
        verify(database, times(1)).searchQueryDao()
        verify(searchQueryDao, times(1)).insert(SearchQuery(query = "query"))
        verifyNoMoreInteractions(api)
        verifyNoMoreInteractions(database)
        verifyNoMoreInteractions(searchQueryDao)
    }

    @Test
    fun `should fail to search given that api returns an error`() {
        val error = Exception()
        whenever(api.search(anyString())).thenReturn(Single.error(error))
        doNothing().whenever(searchQueryDao).insert(any())
        repository.search("query").test()
            .assertError(error)
            .assertNoValues()
            .assertNotComplete()
        verify(api, times(1)).search("query")
        verify(database, times(1)).searchQueryDao()
        verify(searchQueryDao, times(1)).insert(SearchQuery(query = "query"))
        verifyNoMoreInteractions(api)
        verifyNoMoreInteractions(database)
        verifyNoMoreInteractions(searchQueryDao)
    }

    @Test
    fun `should get search history given that database returns as expected`() {
        val searchHistory = listOf(SearchQuery(query = "query"))
        val searchHistoryStrings = listOf("query")
        doReturn(searchHistory).whenever(searchQueryDao).getAll()
        repository.getSearchHistory().test()
            .assertNoErrors()
            .assertValue(searchHistoryStrings)
            .assertComplete()
        verify(database, times(1)).searchQueryDao()
        verify(searchQueryDao, times(1)).getAll()
        verifyZeroInteractions(api)
        verifyNoMoreInteractions(database)
        verifyNoMoreInteractions(searchQueryDao)
    }

    @Test
    fun `should fail to get search history given that database returns an error`() {
        val error = RuntimeException()
        doThrow(error).whenever(searchQueryDao).getAll()
        repository.getSearchHistory().test()
            .assertError(error)
            .assertNoValues()
            .assertNotComplete()
        verify(database, times(1)).searchQueryDao()
        verify(searchQueryDao, times(1)).getAll()
        verifyZeroInteractions(api)
        verifyNoMoreInteractions(database)
        verifyNoMoreInteractions(searchQueryDao)
    }

    @Test
    fun `should delete search query given that database returns no error`() {
        doNothing().whenever(searchQueryDao).delete(anyString())
        repository.deleteSearchQuery("query").test()
            .assertNoErrors()
            .assertNoValues()
            .assertComplete()
        verify(database, times(1)).searchQueryDao()
        verify(searchQueryDao, times(1)).delete("query")
        verifyZeroInteractions(api)
        verifyNoMoreInteractions(database)
        verifyNoMoreInteractions(searchQueryDao)
    }

    @Test
    fun `should fail to delete search query given that database returns an error`() {
        val error = RuntimeException()
        doThrow(error).whenever(searchQueryDao).delete(anyString())
        repository.deleteSearchQuery("query").test()
            .assertError(error)
            .assertNoValues()
            .assertNotComplete()
        verify(database, times(1)).searchQueryDao()
        verify(searchQueryDao, times(1)).delete("query")
        verifyZeroInteractions(api)
        verifyNoMoreInteractions(database)
        verifyNoMoreInteractions(searchQueryDao)
    }
}
