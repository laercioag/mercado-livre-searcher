package com.laercioag.mercadolivresearcher.mapper

import com.laercioag.mercadolivresearcher.data.remote.dto.InstallmentsDTO
import com.laercioag.mercadolivresearcher.data.remote.dto.ProductDTO
import com.laercioag.mercadolivresearcher.data.remote.dto.ShippingDTO
import com.laercioag.mercadolivresearcher.usecase.mapper.ProductMapperImpl
import com.laercioag.mercadolivresearcher.usecase.model.Product
import junit.framework.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class ProductMapperTest {

    private val mapper = ProductMapperImpl()

    @Test
    fun `should map ProductDTO to Product`() {
        val productDTO = ProductDTO(
            id = "1",
            title = "Product",
            thumbnail = "www.product-thumbnail.com",
            permalink = "www.mercadolivre/product/1",
            currencyId = "BRL",
            price = 500.0,
            installments = InstallmentsDTO(
                quantity = 5,
                amount = 100.0,
                currencyId = "BRL"
            ),
            shipping = ShippingDTO(freeShipping = true)
        )
        val product = Product(
            id = "1",
            title = "Product",
            thumbnail = "www.product-thumbnail.com",
            permalink = "www.mercadolivre/product/1",
            price = "R$ 500,00",
            freeShipping = true,
            installmentQuantity = 5,
            installmentPrice = "R$ 100,00"
        )
        val result = mapper.mapTo(productDTO)
        assertEquals(product, result)
    }
}