package com.laercioag.mercadolivresearcher.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.laercioag.mercadolivresearcher.ui.search.SearchViewModel
import com.laercioag.mercadolivresearcher.usecase.DeleteSearchHistoryUseCase
import com.laercioag.mercadolivresearcher.usecase.GetSearchHistoryUseCase
import com.laercioag.mercadolivresearcher.usecase.SearchUseCase
import com.laercioag.mercadolivresearcher.usecase.model.Product
import com.nhaarman.mockitokotlin2.*
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.ArgumentMatchers.anyString

@RunWith(JUnit4::class)
class SearchViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()
    private val searchUseCase: SearchUseCase = mock()
    private val getSearchHistoryUseCase: GetSearchHistoryUseCase = mock()
    private val deleteSearchHistoryUseCase: DeleteSearchHistoryUseCase = mock()
    private val searchInputObserver: Observer<String> = mock()
    private val searchResultStateObserver: Observer<SearchViewModel.SearchResultState> = mock()
    private val searchHistoryStateObserver: Observer<SearchViewModel.SearchHistoryState> = mock()
    private lateinit var searchViewModel: SearchViewModel

    @Before
    fun setup() {
        searchViewModel = SearchViewModel(
            searchUseCase,
            getSearchHistoryUseCase,
            deleteSearchHistoryUseCase
        ).apply {
            searchInput.observeForever(searchInputObserver)
            searchResultState.observeForever(searchResultStateObserver)
            searchHistoryState.observeForever(searchHistoryStateObserver)
        }
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
        RxAndroidPlugins.setMainThreadSchedulerHandler { Schedulers.trampoline() }
    }

    @After
    fun after() {
        RxAndroidPlugins.reset()
    }

    @Test
    fun `search result state should be Success given that search returns products`() {
        val products = listOf(mock<Product>())
        whenever(searchUseCase.execute(any())).thenReturn(Single.just(products))
        searchViewModel.searchInput.value = "query"
        verify(searchInputObserver).onChanged("query")
        verify(searchUseCase).execute("query")
        verify(searchResultStateObserver).onChanged(SearchViewModel.SearchResultState.Loading)
        verify(searchResultStateObserver).onChanged(
            SearchViewModel.SearchResultState.Success(products)
        )
        verifyNoMoreInteractions(searchUseCase)
        verifyNoMoreInteractions(searchInputObserver)
        verifyNoMoreInteractions(searchResultStateObserver)
        verifyZeroInteractions(searchHistoryStateObserver)
        verifyZeroInteractions(getSearchHistoryUseCase)
        verifyZeroInteractions(deleteSearchHistoryUseCase)
    }

    @Test
    fun `search result state should be Empty given that search returns no products`() {
        val products = listOf<Product>()
        whenever(searchUseCase.execute(any())).thenReturn(Single.just(products))
        searchViewModel.searchInput.value = "query"
        verify(searchInputObserver).onChanged("query")
        verify(searchUseCase).execute("query")
        verify(searchResultStateObserver).onChanged(SearchViewModel.SearchResultState.Loading)
        verify(searchResultStateObserver).onChanged(
            SearchViewModel.SearchResultState.Empty
        )
        verifyNoMoreInteractions(searchUseCase)
        verifyNoMoreInteractions(searchInputObserver)
        verifyNoMoreInteractions(searchResultStateObserver)
        verifyZeroInteractions(searchHistoryStateObserver)
        verifyZeroInteractions(getSearchHistoryUseCase)
        verifyZeroInteractions(deleteSearchHistoryUseCase)
    }

    @Test
    fun `search result state should be Error given that search returns an error`() {
        val error = Exception()
        whenever(searchUseCase.execute(any())).thenReturn(Single.error(error))
        searchViewModel.searchInput.value = "query"
        verify(searchInputObserver).onChanged("query")
        verify(searchUseCase).execute("query")
        verify(searchResultStateObserver).onChanged(SearchViewModel.SearchResultState.Loading)
        verify(searchResultStateObserver).onChanged(
            SearchViewModel.SearchResultState.Error(error)
        )
        verifyNoMoreInteractions(searchUseCase)
        verifyNoMoreInteractions(searchInputObserver)
        verifyNoMoreInteractions(searchResultStateObserver)
        verifyZeroInteractions(searchHistoryStateObserver)
        verifyZeroInteractions(getSearchHistoryUseCase)
        verifyZeroInteractions(deleteSearchHistoryUseCase)
    }

    @Test
    fun `search history state should be Success given that get history returns a history`() {
        val searchHistory = listOf("query")
        whenever(getSearchHistoryUseCase.execute()).thenReturn(Single.just(searchHistory))
        searchViewModel.getSearchHistory()
        verify(searchHistoryStateObserver).onChanged(
            SearchViewModel.SearchHistoryState.Success(searchHistory)
        )
        verify(getSearchHistoryUseCase).execute()
        verifyNoMoreInteractions(searchHistoryStateObserver)
        verifyZeroInteractions(searchUseCase)
        verifyZeroInteractions(searchInputObserver)
        verifyZeroInteractions(searchResultStateObserver)
        verifyZeroInteractions(deleteSearchHistoryUseCase)
    }

    @Test
    fun `search history state should be Error given that get history returns an error`() {
        val error = Exception()
        whenever(getSearchHistoryUseCase.execute()).thenReturn(Single.error(error))
        searchViewModel.getSearchHistory()
        verify(searchHistoryStateObserver).onChanged(
            SearchViewModel.SearchHistoryState.Error(error)
        )
        verify(getSearchHistoryUseCase).execute()
        verifyNoMoreInteractions(searchHistoryStateObserver)
        verifyZeroInteractions(searchUseCase)
        verifyZeroInteractions(searchInputObserver)
        verifyZeroInteractions(searchResultStateObserver)
        verifyZeroInteractions(deleteSearchHistoryUseCase)
    }

    @Test
    fun `should get search history given that delete search history is successful`() {
        val searchHistory = listOf("query")
        whenever(deleteSearchHistoryUseCase.execute(anyString())).thenReturn(Completable.complete())
        whenever(getSearchHistoryUseCase.execute()).thenReturn(Single.just(searchHistory))
        searchViewModel.deleteSearchHistory("query")
        verify(deleteSearchHistoryUseCase).execute("query")
        verify(searchHistoryStateObserver).onChanged(
            SearchViewModel.SearchHistoryState.Success(searchHistory)
        )
        verify(getSearchHistoryUseCase).execute()
        verifyNoMoreInteractions(searchHistoryStateObserver)
        verifyZeroInteractions(searchUseCase)
        verifyZeroInteractions(searchInputObserver)
        verifyZeroInteractions(searchResultStateObserver)
    }

    @Test
    fun `should do nothing given that delete search history fails`() {
        val error = Exception()
        whenever(deleteSearchHistoryUseCase.execute(anyString())).thenReturn(Completable.error(error))
        searchViewModel.deleteSearchHistory("query")
        verify(deleteSearchHistoryUseCase).execute("query")
        verifyZeroInteractions(getSearchHistoryUseCase)
        verifyZeroInteractions(searchHistoryStateObserver)
        verifyZeroInteractions(searchUseCase)
        verifyZeroInteractions(searchInputObserver)
        verifyZeroInteractions(searchResultStateObserver)
    }
}