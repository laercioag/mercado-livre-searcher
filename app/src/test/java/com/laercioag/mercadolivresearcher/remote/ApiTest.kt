package com.laercioag.mercadolivresearcher.remote

import com.laercioag.mercadolivresearcher.data.remote.api.ApiImpl
import com.laercioag.mercadolivresearcher.data.remote.service.RemoteService
import junit.framework.TestCase.assertEquals
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File

@RunWith(JUnit4::class)
class ApiTest {

    private val mockWebServer = MockWebServer()
    private val retrofit = Retrofit.Builder()
        .baseUrl(mockWebServer.url("/"))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())
        .build()
    private val service = retrofit.create(RemoteService::class.java)
    private val api = ApiImpl(service)

    @After
    fun after() {
        mockWebServer.shutdown()
    }

    @Test
    fun `should complete and give no error given that request successfully given that servers returns 200`() {
        val query = "galaxy s20"
        val path = "/MLB/search?q=galaxy%20s20"
        val mockResponse = MockResponse()
            .setResponseCode(200)
            .setBody(getJson("json/search_response.json"))
        mockWebServer.enqueue(mockResponse)
        api.search(query)
            .test()
            .assertNoErrors()
            .assertComplete()
        val request = mockWebServer.takeRequest()
        assertEquals(path, request.path)
    }

    @Test
    fun `should not complete and have no values given that request successfully given that servers returns 500`() {
        val query = "galaxy s20"
        val path = "/MLB/search?q=galaxy%20s20"
        val mockResponse = MockResponse()
            .setResponseCode(500)
            .setBody(getJson("json/search_response.json"))
        mockWebServer.enqueue(mockResponse)
        api.search(query)
            .test()
            .assertNoValues()
            .assertNotComplete()
        val request = mockWebServer.takeRequest()
        assertEquals(path, request.path)
    }

    @Suppress("SameParameterValue")
    private fun getJson(path: String): String {
        val uri = this::class.java.classLoader!!.getResource(path)
        val file = File(uri.path)
        return String(file.readBytes())
    }
}