# Mercado Livre Searcher
Buscador de produtos no Mercado Livre.
Esse projeto é uma demonstração técnica e consiste em uma aplicação que utilizando a API pública do Mercado Livre buscar por produtos e os exibe em uma lista/detalhe.

# Sobre
O projeto tem as seguintes definições de arquitetura e tecnologia:
- Clean Architecture com MVVM
- Ktlint
- Kotlin
- RxJava 2
- Dagger 2
- jUnit / Mockito
- Android Jetpack: Room, Navigation, Live Data e View Model

# Melhorias
- A arquitetura do projeto pode ser melhorada a partir do isolamento das camadas (data/usecase/ui) em módulos e se necessário em partes menores, de forma a garantir que as camadas mais baixas não tenham acesso as camadas mais altas e garantino que não haja mal uso ou máspráticas.
- A listagem dos resultados de busca pode ser melhorarada com a implementação de uma paginação, no momento só são exibidos os 50 primeiros resultados.
